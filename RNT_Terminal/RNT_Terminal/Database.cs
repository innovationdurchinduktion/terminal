﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows;
using System.Security.Cryptography;
using System.Configuration;

namespace RNT_Terminal
{
    static class Database
    {
        static MySqlConnection conn = new MySqlConnection(ConfigurationManager.AppSettings["connString"]);
        public static string Salt(string password)
        {
            return "a" + password;
        }
        public static string ByteArrayToHexString(byte[] b)
        {
            var hex = BitConverter.ToString(b);
            return hex.Replace("-", string.Empty);
        }
        public static bool TestConnection()
        {
            try
            {
                conn.Open();
                conn.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static void RefreshConn()
        {
            conn = new MySqlConnection(ConfigurationManager.AppSettings["connString"]);
        }

        #region Queries
        public static List<Item> GetItems(string where)
        {
            List<Item> output = new List<Item>();
            var query = "SELECT iid, name, description, isActive, i_rfid FROM item LEFT JOIN rent USING(iid) GROUP BY iid HAVING SUM(returned_date IS NULL XOR from_date IS NULL) = 0";
            query += where != string.Empty ? " WHERE " + where : string.Empty;
            var cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var temp = new Item(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetBoolean(3), !reader.IsDBNull(4) ? reader.GetString(4) : string.Empty);
                        output.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return output;
        }
        public static List<RentedItem> GetRentedItems(string where)
        {
            List<RentedItem> output = new List<RentedItem>();
            var query = "SELECT iid, name, description, due_date, i_rfid FROM item JOIN rent USING(iid) WHERE returned_date IS NULL";
            query += where != string.Empty ? "AND " + where : string.Empty;
            var cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var temp = new RentedItem(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), !reader.IsDBNull(4) ? reader.GetString(4) : string.Empty);
                        output.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return output;
        }
        public static List<Curator> GetCurators(string where)
        {
            List<Curator> output = new List<Curator>();
            var query = "SELECT cid, fname, lname, email_address, c_rfid FROM curator";
            query += where != string.Empty ? " WHERE " + where : string.Empty;
            var cmd = new MySqlCommand(query, conn);
            try
            {
                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var temp = new Curator(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.IsDBNull(4) ? string.Empty : reader.GetString(4));
                        output.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return output;
        }
        public static Keeper GetKeeper(int iid)
        {
            Keeper keeper = Keeper.Empty;
            var query = "SELECT fname, lname, email_address FROM rent WHERE returned_date IS NULL AND iid = @iid";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@iid", iid);
            try
            {
                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    keeper = new Keeper(reader.GetString(0), reader.GetString(1), reader.GetString(2));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return keeper;
        }
        // Implement further
        public static Curator GetCurator(string cardId)
        {
            Curator c = null;
            var query = "SELECT cid, fname, lname, email_address, c_rfid FROM curator WHERE c_rfid = @cardId";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@cardId", cardId);
            try
            {
                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        c = new Curator(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.IsDBNull(4) ? string.Empty : reader.GetString(4));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return c;
        }
        public static Curator GetCurator(string email, string password)
        {
            var md5 = MD5.Create();
            var pwHash = md5.ComputeHash(Encoding.UTF8.GetBytes(Salt(password)));
            Curator c = null;
            var query = "SELECT cid, fname, lname, email_address, c_rfid FROM curator WHERE email_address = @email AND password = @pwhash";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@pwhash", ByteArrayToHexString(pwHash));
            try
            {
                conn.Open();
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        c = new Curator(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.IsDBNull(4) ? string.Empty : reader.GetString(4));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return c;
        }
        #endregion
        #region Inserts
        public static void UpdateItem(Item item)
        {
            var query = "UPDATE item SET name = @name, description = @desc, isActive = @active, i_rfid = " + (item.RFID.Length > 0 ? "@rfid" : "NULL") + " WHERE iid = @iid";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", item.Name);
            cmd.Parameters.AddWithValue("@desc", item.Description);
            cmd.Parameters.AddWithValue("@active", item.IsActive);
            cmd.Parameters.AddWithValue("@iid", item.ID);
            cmd.Parameters.AddWithValue("@rfid", item.RFID);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        public static void AddItem(Item item, Curator admin)
        {
            var query = "INSERT INTO item (name, description, isActive, addedby_cid, add_date, i_rfid) VALUES (@name, @desc, @active, @by, @date, " + (item.RFID.Length > 0 ? "@rfid" : "NULL") + ")";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", item.Name);
            cmd.Parameters.AddWithValue("@desc", item.Description);
            cmd.Parameters.AddWithValue("@active", item.IsActive);
            cmd.Parameters.AddWithValue("@by", admin.ID);
            cmd.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
            cmd.Parameters.AddWithValue("@rfid", item.RFID);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        public static void LendItem(Item item, Curator admin, DateTime due, string name, string surname, string email)
        {
            var query = "INSERT INTO rent (cid, fname, lname, email_address, from_date, due_date, iid) VALUES (@admin, @name, @surname, @email, @from, @due, @item)";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@admin", admin.ID);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@surname", surname);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@from", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
            cmd.Parameters.AddWithValue("@due", due.ToString("yyyy-MM-dd hh:mm:ss"));
            cmd.Parameters.AddWithValue("@item", item.ID);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        public static void ChangeCurator(int id, string name, string surname, string email, string password, string cardId)
        {
            var md5 = MD5.Create();
            var pwHash = md5.ComputeHash(Encoding.UTF8.GetBytes(Salt(password)));
            var query = "UPDATE curator SET fname = @name, lname = @surname, email_address = @email" + (password.Length > 0 ? ", password = @pw" : string.Empty) + ", c_rfid = " + (cardId.Length > 0 ? "@cardId" : "NULL") + " WHERE cid = @id";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@surname", surname);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@cardId", cardId);
            if (password.Length > 0)
                cmd.Parameters.AddWithValue("@pw", ByteArrayToHexString(md5.ComputeHash(Encoding.UTF8.GetBytes(Salt(password)))));
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        public static void AddCurator(string name, string surname, string email, string password, string cardId)
        {
            var md5 = MD5.Create();
            var pwHash = md5.ComputeHash(Encoding.UTF8.GetBytes(Salt(password)));
            var query = "INSERT INTO curator (fname, lname, email_address, password, c_rfid) VALUES (@name, @surname, @email, @pw, @cardId)";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@surname", surname);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@cardId", cardId);
            if (password.Length > 0)
                cmd.Parameters.AddWithValue("@pw", ByteArrayToHexString(md5.ComputeHash(Encoding.UTF8.GetBytes(Salt(password)))));
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        public static bool RemoveCurator(int id)
        {
            var query = "DELETE FROM curator WHERE cid = @id AND NOT EXISTS (SELECT * FROM rent r WHERE r.cid = @id);";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@id", id);
            try
            {
                conn.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public static bool RemoveItem(int id)
        {
            var query = "DELETE FROM item WHERE iid = @id AND NOT EXISTS(SELECT * FROM rent r WHERE r.iid = @id);";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@id", id);
            try
            {
                conn.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
            return false;
        }
        public static void ReturnItem(int iid, DateTime date)
        {
            var query = "UPDATE rent SET returned_date = @date WHERE returned_date IS NULL AND iid = @id";
            var cmd = new MySqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@id", iid);
            cmd.Parameters.AddWithValue("@date", date.ToString("yyyy-MM-dd hh:mm:ss"));
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion
    }
}