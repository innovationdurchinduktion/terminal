﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace RNT_Terminal
{
    public static class EmailManager
    {
        public static bool SendReminderTo(string addr, RentedItem item)
        {
            try
            {
                SmtpClient client = new SmtpClient(ConfigurationManager.AppSettings["smptServer"])
                {
                    UseDefaultCredentials = false,
                    EnableSsl = true,
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["emailUser"], ConfigurationManager.AppSettings["emailPassword"])
                };
                MailAddress sender = new MailAddress(ConfigurationManager.AppSettings["emailAddress"]);
                MailAddress receiver = new MailAddress(addr);

                MailMessage mail = new MailMessage(sender, receiver);
                mail.Subject = "Item overdue - " + item.Name;
                mail.Body = $"This message aims to remind you to return the item '{item.Name}'.{Environment.NewLine}This should have been done at {item.DueDate}.{Environment.NewLine}{Environment.NewLine}Please do not reply to this message.";
                mail.SubjectEncoding = Encoding.UTF8;
                mail.BodyEncoding = Encoding.UTF8;

                client.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
