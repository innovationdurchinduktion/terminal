﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNT_Terminal
{
    public class Item
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string RFID { get; set; }
        public string Description { get; set; }
        public Item (int id, string name, string desc, bool active, string rfid)
        {
            ID = id;
            Name = name;
            Description = desc;
            IsActive = active;
            RFID = rfid;
        }
    }
    public class RentedItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Date DueDate { get; set; }
        public bool Overdue { get; set; }
        public string RFID { get; set; }
        public string Description { get; set; }
        public RentedItem (int id, string name, string desc, DateTime due, string rfid)
        {
            ID = id;
            Name = name;
            Description = desc;
            DueDate = Date.FromDateTime(due);
            Overdue = DateTime.Compare(DateTime.Now, DueDate.ToDateTime()) > 0 ? true : false;
            RFID = rfid;
        }
    }
    public class Date : IComparable
    {
        int _day;
        int _month;
        int _year;
        public int CompareTo(object obj)
        {
            if (obj is Date)
                return ToDateTime().CompareTo(((Date)obj).ToDateTime());
            else
                return 0;
        }
        public static Date FromDateTime(DateTime dt)
        {
            return new Date
            {
                _day = dt.Day,
                _month = dt.Month,
                _year = dt.Year
            };
        }
        public DateTime ToDateTime()
        {
            return new DateTime(_year, _month, _day);
        }
        public override string ToString()
        {
            return $"{_day}.{_month}.{_year}";
        }
    }
}
