﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNT_Terminal
{
    class Keeper
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public Keeper(string n, string ln, string email)
        {
            Name = n;
            LastName = ln;
            EmailAddress = email;
        }
        public static Keeper Empty
        {
            get
            {
                return new Keeper(string.Empty, string.Empty, string.Empty);
            }
        }
    }
}
