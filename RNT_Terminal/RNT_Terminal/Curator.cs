﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RNT_Terminal
{
    class Curator
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string EmailAddress { get; set; }
        public string CardId { get; set; }
        public Curator(int id, string name, string sname, string email, string cardId)
        {
            ID = id;
            Name = name;
            Surname = sname;
            EmailAddress = email;
            CardId = cardId;
        }
    }
}
