﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MaterialDesignThemes.Wpf;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Net;
using System.Threading;
using uFRSimplest;

//  Todo


//  To be tested
//      Add email notification

//  Done
//      Add data source configuration
//      Finish filters
//      Add rfid
//      Add rfid options for entities
//          Modify GetItem      -
//          Modify AddItem      -
//          Modify ChangeItem   -

namespace RNT_Terminal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Thread nfc_thread;
        event NfcEventHandler CardDetected;

        bool isLoginShown = false;
        static Curator currentAdmin;

        List<Item> available = new List<Item>();
        List<RentedItem> unavailable = new List<RentedItem>();
        List<Curator> curators = new List<Curator>();
        public delegate void NfcEventHandler(object sender, NfcEventArgs e);
        Button cmd_cancel
        {
            get
            {
                var cmd = new Button() { Content = "Cancel", Margin = new Thickness(0, 5, 0, 0) };
                cmd.Click += new RoutedEventHandler((s, re) =>
                {
                    if (CardDetected != null)
                    {
                        foreach (var item in CardDetected.GetInvocationList())
                        {
                            CardDetected -= (NfcEventHandler)item;
                        }
                    }
                    dialogHost_main.IsOpen = false;
                });
                return cmd;
            }
        }
        bool IsValidEmail(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public MainWindow()
        {
            InitializeComponent();
            //Events
            Loaded += MainWindow_Loaded;
        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Title = "RNT - Terminal";
            view_items.IsChecked = true;

            nfc_thread = new Thread(() =>
            {
                //rfid logic
                while (true)
                {
                    Thread.Sleep(500);
                    string prevCard = string.Empty;
                    bool isConnected;

                    while (true)
                    {
                        Thread.Sleep(125);
                        byte uidSize;
                        byte cardType;
                        byte[] cardUID = new byte[9];
                        unsafe
                        {
                            uint temp = uFCoder.ReaderOpen();
                            isConnected = temp == 0;

                            fixed (byte* data = cardUID)
                                if (isConnected)
                                {
                                    uFCoder.GetDlogicCardType(&cardType);
                                    uFCoder.GetCardIdEx(&cardType, data, &uidSize);
                                    string buffer = string.Empty;
                                    for (byte i = 0; i < uidSize; i++)
                                    {
                                        buffer += data[i].ToString("X2");
                                    }
                                    if (buffer != string.Empty && buffer != "00000000" && buffer != prevCard)
                                    {
                                        var cardId = "0x" + buffer;
                                        Application.Current.Dispatcher.Invoke(() =>
                                        {
                                            if (isLoginShown)
                                            {
                                                if ((currentAdmin = Database.GetCurator(cardId)) != null)
                                                {
                                                    login();
                                                }
                                            }
                                            else if (currentAdmin != null && currentAdmin.CardId == cardId)
                                            {
                                                Logout();
                                            }
                                            else
                                            {
                                                if (dialogHost_main.IsOpen)
                                                {
                                                    if (CardDetected != null)
                                                    {
                                                        CardDetected.Invoke(this, new NfcEventArgs(cardId));
                                                    }
                                                }
                                                else if (view_items.IsChecked)
                                                {
                                                    var a = available.Where((i) => i.RFID == cardId).ToArray();
                                                    if (a.Length == 1)
                                                    {
                                                        LendItem(a[0]);
                                                    }
                                                    else
                                                    {
                                                        var u = unavailable.Where((i) => i.RFID == cardId).ToArray();
                                                        if (u.Length == 1)
                                                        {
                                                            ReturnItem(u[0]);
                                                        }
                                                    }
                                                }
                                            }
                                        });


                                    }
                                    prevCard = buffer;
                                }
                        }
                    }
                }
            });
            nfc_thread.IsBackground = true;
            nfc_thread.Start();

            ShowLogin();
        }
        private void login()
        {
            refreshData();
            dialogHost_main.IsOpen = false;
            isLoginShown = false;
            lbl_curAdmin.Content = "Logged in as " + currentAdmin.EmailAddress;
        }
        void refreshData()
        {
            refreshItems();
            refreshCurators();
        }
        void getItemsFromDatabase()
        {
            available = Database.GetItems(string.Empty);
            unavailable = Database.GetRentedItems(string.Empty);
        }
        void getCuratorsFromDatabase()
        {
            curators = Database.GetCurators(string.Empty);
        }
        void filterItems()
        {
            dg_available.ItemsSource = null;
            dg_unavailable.ItemsSource = null;
            dg_available.ItemsSource = available.Where(i =>
            {
                try
                {
                    return
                        (txt_filter_item_name.Text != string.Empty ? Regex.IsMatch(i.Name, txt_filter_item_name.Text) : true) &&
                        (txt_filter_item_desc.Text != string.Empty ? Regex.IsMatch(i.Description, txt_filter_item_desc.Text) : true);
                }
                catch
                {
                    return true;
                }
            });
            dg_unavailable.ItemsSource = unavailable.Where(i =>
            {
                try
                {
                    return
                        (txt_filter_item_name.Text != string.Empty ? Regex.IsMatch(i.Name, txt_filter_item_name.Text) : true) &&
                        (txt_filter_item_desc.Text != string.Empty ? Regex.IsMatch(i.Description, txt_filter_item_desc.Text) : true);
                }
                catch
                {
                    return true;
                }
            });
        }
        void filterCurators()
        {
            dg_curators.ItemsSource = null;
            dg_curators.ItemsSource = curators.Where(c =>
            {
                try
                {
                    return
                    (txt_filter_curator_name.Text != string.Empty ? Regex.IsMatch(c.Name, txt_filter_curator_name.Text) || Regex.IsMatch(c.Surname, txt_filter_curator_name.Text) : true) &&
                    (txt_filter_curator_email.Text != string.Empty ? Regex.IsMatch(c.EmailAddress, txt_filter_curator_email.Text) : true);
                }
                catch
                {
                    return true;
                }
            });
        }
        void refreshItems()
        {
            getItemsFromDatabase();
            filterItems();
        }
        void refreshCurators()
        {
            getCuratorsFromDatabase();
            filterCurators();
        }
        void ShowLogin()
        {
            isLoginShown = true;
            TextBox txt_email;
            PasswordBox pb_password;
            var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
            stack.Children.Add(new Label() { Content = "Email address" });
            stack.Children.Add(txt_email = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            stack.Children.Add(new Label() { Content = "Password" });
            stack.Children.Add(pb_password = new PasswordBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            var cmd_accept = new Button() { Content = "Login", Margin = new Thickness(0d, 0d, 0d, 16d) };
            cmd_accept.Click += new RoutedEventHandler((s, re) =>
            {
                if (txt_email.Text != string.Empty && pb_password.Password != string.Empty)
                {
                    if ((currentAdmin = Database.GetCurator(txt_email.Text, pb_password.Password)) != null)
                    {
                        login();
                    }
                    else
                    {
                        pb_password.Password = string.Empty;
                        txt_email.Text = string.Empty;
                    }
                }
            });
            stack.Children.Add(cmd_accept);
            var cmd_changeDb = new Button() { Content = "Change database" };
            cmd_changeDb.Click += new RoutedEventHandler((s, re) =>
            {
                TextBox txt_ip;
                TextBox txt_db;
                TextBox txt_user;
                PasswordBox pb_db_pass;
                var newStack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                newStack.Children.Add(new Label() { Content = "IP Address" });
                newStack.Children.Add(txt_ip = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
                newStack.Children.Add(new Label() { Content = "Database" });
                newStack.Children.Add(txt_db = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
                newStack.Children.Add(new Label() { Content = "User" });
                newStack.Children.Add(txt_user = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
                newStack.Children.Add(new Label() { Content = "Password" });
                newStack.Children.Add(pb_db_pass = new PasswordBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
                var cmd_changeConnString = new Button() { Content = "Change database", Margin = new Thickness(0d, 0d, 0d, 16d) };
                cmd_changeConnString.Click += new RoutedEventHandler((s1, re1) =>
                {
                    if (IPAddress.TryParse(txt_ip.Text, out var x_unused) && txt_db.Text != string.Empty && txt_user.Text != string.Empty)
                    {
                        ConfigurationManager.AppSettings["connString"] = $"Server={txt_ip.Text};Database={txt_db.Text};Uid={txt_user.Text};Pwd={pb_db_pass.Password};";
                        Database.RefreshConn();
                    }
                });
                newStack.Children.Add(cmd_changeConnString);
                var cmd_testConn = new Button() { Content = "Test connection", Margin = new Thickness(0d, 0d, 0d, 16d) };
                cmd_testConn.Click += new RoutedEventHandler((s1, re1) =>
                {
                    MessageBox.Show(Database.TestConnection() ? "Connection active" : "Connection inactive");
                });
                newStack.Children.Add(cmd_testConn);
                var cmd_return = new Button() { Content = "Back" };
                cmd_return.Click += new RoutedEventHandler((s1, re1) =>
                {
                    dialogHost_main.IsOpen = false;
                    ShowLogin();
                });
                newStack.Children.Add(cmd_return);
                isLoginShown = false;
                dialogHost_main.IsOpen = false;
                dialogHost_main.ShowDialog(newStack);
            });
            stack.Children.Add(cmd_changeDb);
            dialogHost_main.ShowDialog(stack);
        }

        private void dg_available_lend_Click(object sender, RoutedEventArgs e)
        {
            if (dg_available.SelectedIndex != -1)
            {
                LendItem((Item)dg_available.SelectedItem);
            }
        }
        private void LendItem(Item item)
        {
            if (item.IsActive)
            {
                TextBox txt_name;
                TextBox txt_surname;
                TextBox txt_email;
                DatePicker dp_date;
                var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                stack.Children.Add(new Label() { Content = "First name" });
                stack.Children.Add(txt_name = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
                stack.Children.Add(new Label() { Content = "Last name" });
                stack.Children.Add(txt_surname = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
                stack.Children.Add(new Label() { Content = "Email address" });
                stack.Children.Add(txt_email = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
                stack.Children.Add(new Label() { Content = "Due date" });
                stack.Children.Add(dp_date = new DatePicker() { Margin = new Thickness(0d, 0d, 0d, 16d) });
                var cmd_accept = new Button() { Content = "Accept" };
                cmd_accept.Click += new RoutedEventHandler((s, re) =>
                {
                    if (txt_name.Text != string.Empty && txt_surname.Text != string.Empty && IsValidEmail(txt_email.Text) && dp_date.SelectedDate.HasValue && dp_date.SelectedDate.Value.CompareTo(DateTime.Now) > 0)
                    {
                        Database.LendItem(item, currentAdmin, dp_date.SelectedDate.Value, txt_name.Text, txt_surname.Text, txt_email.Text);
                        refreshItems();
                        dialogHost_main.IsOpen = false;
                    }
                });
                stack.Children.Add(cmd_accept);
                stack.Children.Add(cmd_cancel);
                dialogHost_main.ShowDialog(stack);
            }
            else
            {
                var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                stack.Children.Add(new Label() { Content = "Error" });
                stack.Children.Add(new Label() { Margin = new Thickness(0d, 0d, 0d, 16d), Content = "Cannot lend inactive item" });
                stack.Children.Add(cmd_cancel);
                dialogHost_main.ShowDialog(stack);
            }
        }

        private void CloseDialog_Click(object sender, RoutedEventArgs e)
        {
            dialogHost_main.IsOpen = false;
        }

        private void dg_available_change_Click(object sender, RoutedEventArgs e)
        {
            if (dg_available.SelectedIndex != -1)
            {
                TextBox txt_name;
                TextBox txt_desc;
                CheckBox cb_isActive;
                TextBox txt_nfc;
                var selection = (Item)dg_available.SelectedItem;
                var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                stack.Children.Add(new Label() { Content = "Name" });
                stack.Children.Add(txt_name = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = selection.Name });
                stack.Children.Add(new Label() { Content = "Description" });
                stack.Children.Add(txt_desc = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = selection.Description, TextWrapping = TextWrapping.Wrap, AcceptsReturn = true, TextAlignment = TextAlignment.Left });
                stack.Children.Add(cb_isActive = new CheckBox() { Content = "Is active", Margin = new Thickness(0, 0, 0, 16d), IsChecked = selection.IsActive });
                stack.Children.Add(new Label() { Content = "RFID" });
                stack.Children.Add(txt_nfc = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = selection.RFID });
                var nfc_eventHandler = new NfcEventHandler((s, ne) =>
                {
                    txt_nfc.Text = ne.CardId;
                });
                var cmd_accept = new Button() { Content = "Accept" };
                cmd_accept.Click += new RoutedEventHandler((s, re) =>
                {
                    if (txt_name.Text.Length < 32 && txt_desc.Text.Length < 8191)
                    {
                        Database.UpdateItem(new Item(selection.ID, txt_name.Text, txt_desc.Text, cb_isActive.IsChecked.Value, txt_nfc.Text));
                        refreshItems();
                        CardDetected -= nfc_eventHandler;
                        dialogHost_main.IsOpen = false;
                    }
                });
                stack.Children.Add(cmd_accept);
                stack.Children.Add(cmd_cancel);
                dialogHost_main.ShowDialog(stack);
                CardDetected += nfc_eventHandler;
            }
        }
        private void dg_unavailable_return_Click(object sender, RoutedEventArgs e)
        {
            if (dg_unavailable.SelectedIndex != -1)
            {
                ReturnItem((RentedItem)dg_unavailable.SelectedItem);
            }
        }
        private void ReturnItem(RentedItem item)
        {
            var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
            DatePicker dp_returned;
            stack.Children.Add(new Label() { Content = "Date" });
            stack.Children.Add(dp_returned = new DatePicker() { SelectedDate = DateTime.Now.Date, Margin = new Thickness(0d, 0d, 0d, 16d) });
            var cmd = new Button() { Content = "Accept" };
            cmd.Click += new RoutedEventHandler((s, re) =>
            {
                Database.ReturnItem(item.ID, dp_returned.SelectedDate.Value);
                refreshItems();
            });
            cmd.Click += CloseDialog_Click;
            stack.Children.Add(cmd);
            stack.Children.Add(cmd_cancel);
            dialogHost_main.ShowDialog(stack);
        }
        private void dg_unavailable_view_keeper_Click(object sender, RoutedEventArgs e)
        {
            if (dg_unavailable.SelectedIndex != -1)
            {
                var keeper = Database.GetKeeper(((RentedItem)dg_unavailable.SelectedItem).ID);
                var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                stack.Children.Add(new Label() { Content = "First name" });
                stack.Children.Add(new Label() { Content = keeper.Name, Margin = new Thickness(0d, 0d, 0d, 16d) });
                stack.Children.Add(new Label() { Content = "Last name" });
                stack.Children.Add(new Label() { Content = keeper.LastName, Margin = new Thickness(0d, 0d, 0d, 16d) });
                stack.Children.Add(new Label() { Content = "Email address" });
                stack.Children.Add(new Label() { Content = keeper.EmailAddress, Margin = new Thickness(0d, 0d, 0d, 16d) });
                stack.Children.Add(cmd_cancel);
                dialogHost_main.ShowDialog(stack);
            }
        }
        private void view_items_Checked(object sender, RoutedEventArgs e)
        {
            gd_items.Visibility = Visibility.Visible;
            gd_curators.Visibility = Visibility.Hidden;
            view_curators.IsChecked = false;
        }
        private void view_curators_Checked(object sender, RoutedEventArgs e)
        {
            gd_curators.Visibility = Visibility.Visible;
            gd_items.Visibility = Visibility.Hidden;
            view_items.IsChecked = false;
        }
        private void view_items_Click(object sender, RoutedEventArgs e)
        {
            if (!view_curators.IsChecked)
                view_items.IsChecked = true;
        }

        private void view_curators_Click(object sender, RoutedEventArgs e)
        {
            if (!view_items.IsChecked)
                view_curators.IsChecked = true;
        }

        private void control_refresh_Click(object sender, RoutedEventArgs e)
        {
            refreshData();
        }

        private void add_item_Click(object sender, RoutedEventArgs e)
        {
            TextBox txt_name;
            TextBox txt_desc;
            CheckBox cb_isActive;
            TextBox txt_nfc;
            var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
            stack.Children.Add(new Label() { Content = "Name" });
            stack.Children.Add(txt_name = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            stack.Children.Add(new Label() { Content = "Description" });
            stack.Children.Add(txt_desc = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), TextWrapping = TextWrapping.Wrap, AcceptsReturn = true, TextAlignment = TextAlignment.Left });
            stack.Children.Add(cb_isActive = new CheckBox() { Content = "Is active", Margin = new Thickness(0, 0, 0, 16d), });
            stack.Children.Add(new Label() { Content = "RFID" });
            stack.Children.Add(txt_nfc = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            var nfc_eventHandler = new NfcEventHandler((s, ne) =>
            {
                txt_nfc.Text = ne.CardId;
            });
            var cmd_accept = new Button() { Content = "Accept" };
            cmd_accept.Click += new RoutedEventHandler((s, re) =>
            {
                if (txt_name.Text.Length < 32 && txt_desc.Text.Length < 8191)
                {
                    Database.AddItem(new Item(0, txt_name.Text, txt_desc.Text, cb_isActive.IsChecked.Value, txt_nfc.Text), currentAdmin);
                    refreshItems();
                    CardDetected -= nfc_eventHandler;
                    dialogHost_main.IsOpen = false;
                }
            });
            stack.Children.Add(cmd_accept);
            stack.Children.Add(cmd_cancel);
            dialogHost_main.ShowDialog(stack);
            CardDetected += nfc_eventHandler;
        }

        private void control_logout_Click(object sender, RoutedEventArgs e)
        {
            Logout();
        }

        private void Logout()
        {
            dg_available.ItemsSource = null;
            dg_unavailable.ItemsSource = null;
            dg_curators.ItemsSource = null;
            currentAdmin = null;
            lbl_curAdmin.Content = string.Empty;
            ShowLogin();
        }

        private void gd_curators_change_Click(object sender, RoutedEventArgs e)
        {
            if (dg_curators.SelectedIndex != -1)
            {
                var cur = (Curator)dg_curators.SelectedItem;
                TextBox txt_name;
                TextBox txt_lastname;
                TextBox txt_email;
                TextBox txt_nfc;
                PasswordBox pw_new;
                var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                stack.Children.Add(new Label() { Content = "First name" });
                stack.Children.Add(txt_name = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = cur.Name });
                stack.Children.Add(new Label() { Content = "Last name" });
                stack.Children.Add(txt_lastname = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = cur.Surname });
                stack.Children.Add(new Label() { Content = "Email" });
                stack.Children.Add(txt_email = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = cur.EmailAddress });
                stack.Children.Add(new Label() { Content = "Password" });
                stack.Children.Add(pw_new = new PasswordBox() { Margin = new Thickness(0d, 0d, 0d, 16d), ToolTip = "Leave empty for no change" });
                stack.Children.Add(new Label() { Content = "NFC ID" });
                stack.Children.Add(txt_nfc = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = cur.CardId });
                var nfc_eventHandler = new NfcEventHandler((s, ne) =>
                {
                    txt_nfc.Text = ne.CardId;
                });
                var cmd_accept = new Button() { Content = "Accept" };
                cmd_accept.Click += new RoutedEventHandler((s, re) =>
                {
                    if (txt_name.Text.Length < 51 && txt_lastname.Text.Length < 51 && txt_email.Text.Length < 51 && IsValidEmail(txt_email.Text) && txt_nfc.Text.Length < 11)
                    {
                        Database.ChangeCurator(cur.ID, txt_name.Text, txt_lastname.Text, txt_email.Text, pw_new.Password, txt_nfc.Text);
                        refreshCurators();
                        CardDetected -= nfc_eventHandler;
                        dialogHost_main.IsOpen = false;
                    }
                });
                stack.Children.Add(cmd_accept);
                stack.Children.Add(cmd_cancel);
                dialogHost_main.ShowDialog(stack);
                CardDetected += nfc_eventHandler;
            }
        }

        private void add_curator_Click(object sender, RoutedEventArgs e)
        {
            TextBox txt_name;
            TextBox txt_lastname;
            TextBox txt_email;
            TextBox txt_nfc;
            PasswordBox pw_new;
            var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
            stack.Children.Add(new Label() { Content = "First name" });
            stack.Children.Add(txt_name = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            stack.Children.Add(new Label() { Content = "Last name" });
            stack.Children.Add(txt_lastname = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            stack.Children.Add(new Label() { Content = "Email" });
            stack.Children.Add(txt_email = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            stack.Children.Add(new Label() { Content = "Password" });
            stack.Children.Add(pw_new = new PasswordBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            stack.Children.Add(new Label() { Content = "NFC ID" });
            stack.Children.Add(txt_nfc = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            var nfc_eventHandler = new NfcEventHandler((s, ne) =>
            {
                txt_nfc.Text = ne.CardId;
            });
            var cmd_accept = new Button() { Content = "Accept" };
            cmd_accept.Click += new RoutedEventHandler((s, re) =>
            {
                if (txt_name.Text.Length < 51 && txt_lastname.Text.Length < 51 && txt_email.Text.Length < 51 && IsValidEmail(txt_email.Text) && txt_nfc.Text.Length < 11)
                {
                    Database.AddCurator(txt_name.Text, txt_lastname.Text, txt_email.Text, pw_new.Password, txt_nfc.Text);
                    refreshCurators();
                    CardDetected -= nfc_eventHandler;
                    dialogHost_main.IsOpen = false;
                }
            });
            stack.Children.Add(cmd_accept);
            stack.Children.Add(cmd_cancel);
            dialogHost_main.ShowDialog(stack);
            CardDetected += nfc_eventHandler;
        }
        private void gd_curators_remove_Click(object sender, RoutedEventArgs e)
        {
            if (dg_curators.SelectedIndex != -1)
            {
                var cur = (Curator)dg_curators.SelectedItem;
                if (cur.ID != currentAdmin.ID)
                {
                    var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                    stack.Children.Add(new Label() { Content = "Are you sure you want to remove" });
                    stack.Children.Add(new Label() { Content = "the selected administrator?" });
                    stack.Children.Add(new Label() { Content = "This cannot be undone", Margin = new Thickness(0d, 0d, 0d, 16d) });
                    var cmd_accept = new Button() { Content = "Accept" };
                    cmd_accept.Click += new RoutedEventHandler((s, re) =>
                    {
                        dialogHost_main.IsOpen = false;
                        if (!Database.RemoveCurator(cur.ID))
                        {
                            var newStack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                            newStack.Children.Add(new Label() { Content = "Cannot remove administrator" });
                            newStack.Children.Add(new Label() { Content = "because he already lended items", Margin = new Thickness(0d, 0d, 0d, 16d) });
                            newStack.Children.Add(cmd_cancel);
                            dialogHost_main.ShowDialog(newStack);
                        }
                        else
                        {
                            refreshCurators();
                        }
                    });
                    stack.Children.Add(cmd_accept);
                    stack.Children.Add(cmd_cancel);
                    dialogHost_main.ShowDialog(stack);
                }
                else
                {
                    var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                    stack.Children.Add(new Label() { Content = "Error" });
                    stack.Children.Add(new Label() { Margin = new Thickness(0d, 0d, 0d, 16d), Content = "Cannot remove yourself" });
                    stack.Children.Add(cmd_cancel);
                    dialogHost_main.ShowDialog(stack);
                }
            }
        }
        private void dg_available_remove_Click(object sender, RoutedEventArgs e)
        {
            if (dg_available.SelectedIndex != -1)
            {
                var item = (Item)dg_available.SelectedItem;
                var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                stack.Children.Add(new Label() { Content = "Are you sure you want to remove" });
                stack.Children.Add(new Label() { Content = "the selected item?" });
                stack.Children.Add(new Label() { Content = "This cannot be undone", Margin = new Thickness(0d, 0d, 0d, 16d) });
                var cmd_accept = new Button() { Content = "Accept" };
                cmd_accept.Click += new RoutedEventHandler((s, re) =>
                {
                    dialogHost_main.IsOpen = false;
                    if (!Database.RemoveItem(item.ID))
                    {
                        var newStack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                        newStack.Children.Add(new Label() { Content = "Cannot remove item because" });
                        newStack.Children.Add(new Label() { Content = "it was already lended", Margin = new Thickness(0d, 0d, 0d, 16d) });
                        newStack.Children.Add(cmd_cancel);
                        dialogHost_main.ShowDialog(newStack);
                    }
                    else
                    {
                        refreshItems();
                    }
                });
                stack.Children.Add(cmd_accept);
                stack.Children.Add(cmd_cancel);
                dialogHost_main.ShowDialog(stack);
            }
        }
        private void txt_filter_item_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (mi_filter_applyImmediately.IsChecked)
                filterItems();
        }

        private void txt_filter_curator_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (mi_filter_applyImmediately.IsChecked)
                filterCurators();
        }

        private void mi_filter_applyImmediately_Click(object sender, RoutedEventArgs e)
        {
            mi_filter_apply.IsEnabled = !mi_filter_applyImmediately.IsChecked;
        }

        private void mi_filter_apply_Click(object sender, RoutedEventArgs e)
        {
            filterItems();
            filterCurators();
        }

        private void dg_unavailable_send_reminder_Click(object sender, RoutedEventArgs e)
        {
            if (dg_unavailable.SelectedIndex != -1)
            {
                var item = (RentedItem)dg_unavailable.SelectedItem;
                if (item.Overdue)
                {
                    Keeper keeper;
                    if (EmailManager.SendReminderTo((keeper = Database.GetKeeper(item.ID)).EmailAddress, item))
                    {
                        MessageBox.Show("Successfully sent");
                    }
                    else
                    {
                        MessageBox.Show("Message could not be sent");
                    }
                }
                else
                {
                    var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
                    stack.Children.Add(new Label() { Content = "Cannot remind lender because" });
                    stack.Children.Add(new Label() { Content = "the item is not overdue", Margin = new Thickness(0d, 0d, 0d, 16d) });
                    stack.Children.Add(cmd_cancel);
                    dialogHost_main.ShowDialog(stack);
                }
            }
        }

        private void mi_edit_settings_email_Click(object sender, RoutedEventArgs e)
        {
            TextBox txt_addr;
            TextBox txt_smtp;
            TextBox txt_user;
            PasswordBox pb_pw;
            var stack = new StackPanel() { Margin = new Thickness(16d), Width = 200 };
            stack.Children.Add(new Label() { Content = "Email address" });
            stack.Children.Add(txt_addr = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = ConfigurationManager.AppSettings["emailAddress"] });
            stack.Children.Add(new Label() { Content = "Smpt server" });
            stack.Children.Add(txt_smtp = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = ConfigurationManager.AppSettings["smtpServer"] });
            stack.Children.Add(new Label() { Content = "Username" });
            stack.Children.Add(txt_user = new TextBox() { Margin = new Thickness(0d, 0d, 0d, 16d), Text = ConfigurationManager.AppSettings["emailUser"] });
            stack.Children.Add(new Label() { Content = "Password" });
            stack.Children.Add(pb_pw = new PasswordBox() { Margin = new Thickness(0d, 0d, 0d, 16d) });
            var cmd_accept = new Button() { Content = "Accept", Margin = new Thickness(0d, 0d, 0d, 16d) };
            cmd_accept.Click += new RoutedEventHandler((s, re) =>
            {
                if (IsValidEmail(txt_addr.Text) && txt_smtp.Text != string.Empty && txt_user.Text != string.Empty && pb_pw.Password != string.Empty)
                {
                    ConfigurationManager.AppSettings["emailAddress"] = txt_addr.Text;
                    ConfigurationManager.AppSettings["smtpServer"] = txt_smtp.Text;
                    ConfigurationManager.AppSettings["emailUser"] = txt_user.Text;
                    ConfigurationManager.AppSettings["emailPassword"] = pb_pw.Password;
                }
            });
            stack.Children.Add(cmd_accept);
            stack.Children.Add(cmd_cancel);
            dialogHost_main.ShowDialog(stack);
        }
    }
}
